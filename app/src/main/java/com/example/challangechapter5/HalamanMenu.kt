package com.example.challangechapter5

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

class HalamanMenu : AppCompatActivity() {
    private lateinit var etPemain: TextView
    private lateinit var etMusuh: TextView
    private lateinit var lawanPemain: ImageView
    private lateinit var lawanKomputer: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_menu)

        etPemain = findViewById(R.id.tulisanPemain)
        etMusuh = findViewById(R.id.tulisanMusuh)

        val name = intent.getStringExtra("mario")

        etPemain.text = "$name vs Pemain"
        etMusuh.text = "$name vs CPU"
        val snackBar = Snackbar.make(findViewById(android.R.id.content),"Selamat Datang $name",Snackbar.LENGTH_INDEFINITE)
        snackBar.setAction("Tutup"){
            snackBar.dismiss()
        }
        snackBar.show()

        lawanPemain = findViewById(R.id.gambar1)
        lawanKomputer = findViewById(R.id.gambar2)

        lawanPemain.setOnClickListener {
            val intent = Intent(this,HalamanSuitPemain::class.java)
            intent.putExtra("mario",name)
            startActivity(intent)
        }

        lawanKomputer.setOnClickListener {
            val intent = Intent(this,HalamanSuitKomputer::class.java)
            intent.putExtra("mario",name)
            startActivity(intent)
        }
    }
}