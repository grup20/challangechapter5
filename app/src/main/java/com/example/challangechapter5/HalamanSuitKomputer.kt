package com.example.challangechapter5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat

class HalamanSuitKomputer : AppCompatActivity(), Callback {

    private lateinit var btnBatu: ImageView
    private lateinit var btnKertas: ImageView
    private lateinit var btnGunting: ImageView
    private lateinit var btnRefresh: ImageView
    private lateinit var dataStatus: ImageView
    private lateinit var musuhBatu: ImageView
    private lateinit var musuhKertas: ImageView
    private lateinit var musuhGunting: ImageView
    private lateinit var gantiNama: TextView
    private lateinit var closeGame: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_suit_komputer)

        btnBatu = findViewById(R.id.batu)
        btnKertas = findViewById(R.id.kertas)
        btnGunting = findViewById(R.id.gunting)
        btnRefresh = findViewById(R.id.refresh)
        dataStatus = findViewById(R.id.versus)
        musuhBatu = findViewById(R.id.batuLawan)
        musuhKertas = findViewById(R.id.kertasLawan)
        musuhGunting = findViewById(R.id.guntingLawan)
        gantiNama = findViewById(R.id.pemain)
        closeGame = findViewById(R.id.exitKomputer)

        closeGame.setOnClickListener {
            finish()
        }

        val name = intent.getStringExtra("mario")
        gantiNama.text = name
        val rumus = LogikaSuit(this)
        val btn = mutableListOf(btnBatu, btnGunting, btnKertas)
        var clicked = true

        repeat(btn.size) {
            btnBatu.setOnClickListener {
                val index = (0..2).random()
                if (clicked) {
                    btnBatu.background = (ContextCompat.getDrawable(this, R.drawable.select))
                    clicked = false
                    when (index) {
                        0 -> {
                            musuhBatu.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Batu", Toast.LENGTH_SHORT).show()
                            rumus.rumusSuit(0, 0)
                        }
                        1 -> {
                            musuhGunting.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Gunting", Toast.LENGTH_SHORT)
                                .show()
                            rumus.rumusSuit(0, 1)
                        }
                        2 -> {
                            musuhKertas.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Kertas", Toast.LENGTH_SHORT)
                                .show()
                            rumus.rumusSuit(0, 2)
                        }
                    }
                } else {
                    Toast.makeText(this, "Harap Reset Dulu", Toast.LENGTH_SHORT).show()
                }
            }
            btnGunting.setOnClickListener {
                val index = (0..2).random()
                if (clicked) {
                    btnGunting.background = (ContextCompat.getDrawable(this, R.drawable.select))
                    clicked = false
                    when (index) {
                        0 -> {
                            musuhBatu.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Batu", Toast.LENGTH_SHORT).show()
                            rumus.rumusSuit(1, 0)
                        }
                        1 -> {
                            musuhGunting.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Gunting", Toast.LENGTH_SHORT)
                                .show()
                            rumus.rumusSuit(1, 1)
                        }
                        2 -> {
                            musuhKertas.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Kertas", Toast.LENGTH_SHORT)
                                .show()
                            rumus.rumusSuit(1, 2)
                        }
                    }
                } else {
                    Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
                }
            }
            btnKertas.setOnClickListener {
                val index = (0..2).random()
                if (clicked) {
                    btnKertas.background = (ContextCompat.getDrawable(this, R.drawable.select))
                    clicked = false
                    when (index) {
                        0 -> {
                            musuhBatu.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Batu", Toast.LENGTH_SHORT).show()
                            rumus.rumusSuit(2, 0)
                        }
                        1 -> {
                            musuhGunting.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Gunting", Toast.LENGTH_SHORT)
                                .show()
                            rumus.rumusSuit(2, 1)
                        }
                        2 -> {
                            musuhKertas.background =
                                (ContextCompat.getDrawable(this, R.drawable.select))
                            Toast.makeText(this, "Pemain 2 Memilih Kertas", Toast.LENGTH_SHORT)
                                .show()
                            rumus.rumusSuit(2, 2)
                        }
                    }
                } else {
                    Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
                }
            }
        }
        btnRefresh.setOnClickListener {
            dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vs))
            btnBatu.setBackgroundResource(R.drawable.batu)
            btnGunting.setBackgroundResource(R.drawable.gunting)
            btnKertas.setBackgroundResource(R.drawable.kertas)
            musuhBatu.setBackgroundResource(R.drawable.batu)
            musuhGunting.setBackgroundResource(R.drawable.gunting)
            musuhKertas.setBackgroundResource(R.drawable.kertas)
            clicked = true
            Toast.makeText(this, "Game sudah di Reset Silahkan Bermain Lagi", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun kirimStatus(status: String) {

        val view = LayoutInflater.from(this).inflate(R.layout.dialog_hasil, null, false)
        val alert = AlertDialog.Builder(this)
        val hasilPemenang = view.findViewById<TextView>(R.id.hasilPemenang)
        val btnMain = view.findViewById<Button>(R.id.mainLagi)
        val btnMenu = view.findViewById<Button>(R.id.kembaliMenu)
        val name = intent.getStringExtra("mario")

        when {
            status.contains("W") -> {
                dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.p1menang))
                val name = intent.getStringExtra("mario")
                hasilPemenang.text = "$name Menang!"
                alert.setView(view)
                alert.setCancelable(false)
                val dialog = alert.create()
                dialog.show()
                btnMain.setOnClickListener {
                    dialog.dismiss()
                }
                btnMenu.setOnClickListener {
                    val intent = Intent(this, HalamanMenu()::class.java)
                    intent.putExtra("mario", name)
                    startActivity(intent)
                }
            }
            status.contains("L") -> {
                dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.p2menang))
                alert.setView(view)
                alert.setCancelable(false)
                val dialog = alert.create()
                dialog.show()
                hasilPemenang.text = "CPU MENANG!"
                btnMain.setOnClickListener {
                    dialog.dismiss()
                }
                btnMenu.setOnClickListener {
                    val intent = Intent(this, HalamanMenu()::class.java)
                    intent.putExtra("mario", name)
                    startActivity(intent)
                }
            }
            status.contains("D") -> {
                dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.draw))
                alert.setView(view)
                alert.setCancelable(false)
                val dialog = alert.create()
                dialog.show()
                hasilPemenang.text = "SERI"
                btnMain.setOnClickListener {
                    dialog.dismiss()
                }
                btnMenu.setOnClickListener {
                    val intent = Intent(this, HalamanMenu()::class.java)
                    intent.putExtra("mario", name)
                    startActivity(intent)
                }
            }
        }
    }
}