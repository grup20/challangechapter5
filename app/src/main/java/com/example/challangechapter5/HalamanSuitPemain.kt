package com.example.challangechapter5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat

class HalamanSuitPemain : AppCompatActivity(), Callback {
    private lateinit var btnBatu: ImageView
    private lateinit var btnKertas: ImageView
    private lateinit var btnGunting: ImageView
    private lateinit var btnRefresh: ImageView
    private lateinit var dataStatus: ImageView
    private lateinit var musuhBatu: ImageView
    private lateinit var musuhKertas: ImageView
    private lateinit var musuhGunting: ImageView
    private lateinit var gantiNama: TextView
    private lateinit var closeGame: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_halaman_suit_pemain)

        btnBatu = findViewById(R.id.batuPlayer)
        btnKertas = findViewById(R.id.kertasPlayer)
        btnGunting = findViewById(R.id.guntingPlayer)
        btnRefresh = findViewById(R.id.refreshPlayer)
        dataStatus = findViewById(R.id.versusPlayer)
        musuhBatu = findViewById(R.id.batuMusuh)
        musuhKertas = findViewById(R.id.kertasMusuh)
        musuhGunting = findViewById(R.id.guntingMusuh)
        gantiNama = findViewById(R.id.player1)
        closeGame = findViewById(R.id.exitPlayer)

        closeGame.setOnClickListener {
            finish()
        }

        val name = intent.getStringExtra("mario")
        gantiNama.text = name

        var clickPemain = true
        var clickMusuh = true

        var player1 = 0
        var player2 = 0

        val controller = LogikaSuit(this)

        btnBatu.setOnClickListener {
            if (clickPemain && clickMusuh) {
                btnBatu.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player1 = 0
                clickPemain = false
                Toast.makeText(this, "$name Melilih Batu", Toast.LENGTH_SHORT).show()
                Log.d(name, "memilih Batu")
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Player 2 Silahkan Memilih", Toast.LENGTH_SHORT).show()
            }
        }

        btnGunting.setOnClickListener {
            if (clickPemain && clickMusuh) {
                btnGunting.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player1 = 1
                clickPemain = false
                Toast.makeText(this, "$name Melilih Batu", Toast.LENGTH_SHORT).show()
                Log.d(name, "memilih Batu")
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Player 2 Silahkan Memilih", Toast.LENGTH_SHORT).show()
            }
        }

        btnKertas.setOnClickListener {
            if (clickPemain && clickMusuh) {
                btnKertas.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player1 = 2
                clickPemain = false
                Toast.makeText(this, "$name Melilih Kertas", Toast.LENGTH_SHORT).show()
                Log.d(name, "Memilih Kertas")
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Player 2 Silahkan Memilih", Toast.LENGTH_SHORT).show()
            }
        }

        musuhKertas.setOnClickListener {
            if (clickPemain && clickMusuh) {
                Toast.makeText(this, "Player 1 Silahkan Pilih terlebih dahulu", Toast.LENGTH_SHORT)
                    .show()
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                musuhKertas.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player2 = 2
                clickMusuh = false
                Toast.makeText(this, "Player 2 Memilih Kertas", Toast.LENGTH_SHORT).show()
                Log.d("Test", "Pesan")
                controller.rumusSuit(player1, player2)
            }
        }

        musuhBatu.setOnClickListener {
            if (clickPemain && clickMusuh) {
                Toast.makeText(this, "Player 1 Silahkan Pilih terlebih dahulu", Toast.LENGTH_SHORT)
                    .show()
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                musuhBatu.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player2 = 0
                clickMusuh = false
                Toast.makeText(this, "Player 2 Memilih Batu", Toast.LENGTH_SHORT).show()
                Log.d(name, "Memilih Batu")
                controller.rumusSuit(player1, player2)
            }
        }

        musuhGunting.setOnClickListener {
            if (clickPemain && clickMusuh) {
                Toast.makeText(this, "$name Silahkan Pilih Terlebih Dahulu", Toast.LENGTH_SHORT)
                    .show()
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                musuhGunting.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player2 = 1
                clickMusuh = false
                Toast.makeText(this, "Player 2 Memilih Gunting", Toast.LENGTH_SHORT).show()
                Log.d(name, "Memilih Gunting")
                controller.rumusSuit(player1, player2)
            }
        }

        musuhKertas.setOnClickListener {
            if (clickPemain && clickMusuh) {
                Toast.makeText(this, "Player 1 Silahkan Pilih terlebih dahulu", Toast.LENGTH_SHORT)
                    .show()
            } else if (!clickPemain && !clickMusuh) {
                Toast.makeText(this, "Harap Reset Kembali", Toast.LENGTH_SHORT).show()
            } else {
                musuhKertas.background = (ContextCompat.getDrawable(this, R.drawable.select))
                player2 = 2
                clickMusuh = false
                Toast.makeText(this, "Player 2 Memilih Kertas", Toast.LENGTH_SHORT).show()
                Log.d(name, "Memilih Kertas")
                controller.rumusSuit(player1, player2)
            }
        }

        btnRefresh.setOnClickListener {
            dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vs))
            btnBatu.setBackgroundResource(R.drawable.batu)
            btnGunting.setBackgroundResource(R.drawable.gunting)
            btnKertas.setBackgroundResource(R.drawable.kertas)
            musuhBatu.setBackgroundResource(R.drawable.batu)
            musuhGunting.setBackgroundResource(R.drawable.gunting)
            musuhKertas.setBackgroundResource(R.drawable.kertas)
            clickPemain = true
            clickMusuh = true
            Toast.makeText(this, "Game sudah di Reset!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun kirimStatus(status: String) {
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_hasil, null, false)
        val alert = AlertDialog.Builder(this)
        val hasilPemenang = view.findViewById<TextView>(R.id.hasilPemenang)
        val btnMain = view.findViewById<Button>(R.id.mainLagi)
        val btnMenu = view.findViewById<Button>(R.id.kembaliMenu)
        val name = intent.getStringExtra("mario")

        when {
            status.contains("W") -> {
                dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.p1menang))
                val name = intent.getStringExtra("mario")
                hasilPemenang.text = "$name Menang!"
                alert.setView(view)
                alert.setCancelable(false)
                val dialog = alert.create()
                dialog.show()
                btnMain.setOnClickListener {
                    dialog.dismiss()
                }
                btnMenu.setOnClickListener {
                    val intent = Intent(this, HalamanMenu()::class.java)
                    intent.putExtra("mario", name)
                    startActivity(intent)
                }
            }
            status.contains("L") -> {
                dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.p2menang))
                alert.setView(view)
                alert.setCancelable(false)
                val dialog = alert.create()
                dialog.show()
                hasilPemenang.text = "PLAYER 2 MENANG!"
                btnMain.setOnClickListener {
                    dialog.dismiss()
                }
                btnMenu.setOnClickListener {
                    val intent = Intent(this, HalamanMenu()::class.java)
                    intent.putExtra("mario", name)
                    startActivity(intent)
                }
            }
            status.contains("D") -> {
                dataStatus.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.draw))
                alert.setView(view)
                alert.setCancelable(false)
                val dialog = alert.create()
                dialog.show()
                hasilPemenang.text = "SERI"
                btnMain.setOnClickListener {
                    dialog.dismiss()
                }
                btnMenu.setOnClickListener {
                    val intent = Intent(this, HalamanMenu()::class.java)
                    intent.putExtra("mario", name)
                    startActivity(intent)
                }
            }
        }
    }
}
