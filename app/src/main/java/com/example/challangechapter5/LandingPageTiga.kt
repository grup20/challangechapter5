package com.example.challangechapter5

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment

class LandingPageTiga : Fragment() {
    private lateinit var et: EditText
    private lateinit var btn: ImageView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_landing_page_tiga, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        et = view.findViewById(R.id.nama)
        btn = view.findViewById(R.id.btn)

        et.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.isNotEmpty() == true) {
                    btn.visibility = View.VISIBLE
                } else {
                    btn.visibility = View.GONE
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        btn.setOnClickListener {
            val nama = et.text.toString()
            val intent = Intent(activity, HalamanMenu::class.java)
            intent.putExtra("mario", nama)
            startActivity(intent)
        }
    }
}