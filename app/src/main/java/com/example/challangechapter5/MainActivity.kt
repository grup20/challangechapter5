package com.example.challangechapter5

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dataFragment = mutableListOf(LandingPageSatu(),LandingPageDua(),LandingPageTiga())
        val adapter = Adapter(this)
        adapter.setData(dataFragment)

        val dotsIndicator = findViewById<DotsIndicator>(R.id.dots_indicator)
        val viewPager = findViewById<ViewPager2>(R.id.vp)
        viewPager.adapter = adapter
        dotsIndicator.setViewPager2(viewPager)
    }
}